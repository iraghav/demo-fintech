<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@page session="true" %>
<html>
<head>
  <title>Login Page</title>
  <style>
    .error {
      padding: 15px;
      margin-bottom: 20px;
      border: 1px solid transparent;
      border-radius: 4px;
      color: #a94442;
      background-color: #f2dede;
      border-color: #ebccd1;
    }

    .msg {
      padding: 15px;
      margin-bottom: 20px;
      border: 1px solid transparent;
      border-radius: 4px;
      color: #31708f;
      background-color: #d9edf7;
      border-color: #bce8f1;
    }

    #login-box {
      width: 350px;
      padding: 20px;
      margin: 100px auto;
      background: #fff;
      -webkit-border-radius: 2px;
      -moz-border-radius: 2px;
      border: 1px solid #000;
    }
  </style>
  <link href="${pageContext.request.contextPath}/webjars/bootstrap/3.3.6/css/bootstrap.min.css"
        rel="stylesheet">
</head>
<body onload='document.loginForm.account_name.focus();'>

<h1><spring:message code="login.header" /></h1>

<div id="login-box">

  <h3>Login with Username and Password</h3>

  <c:if test="${not empty error}">
    <div class="error">${error}</div>
  </c:if>
  <c:if test="${not empty msg}">
    <div class="msg">${msg}</div>
  </c:if>

  <form name='loginForm'
        action="<c:url value='/auth/login_check?targetUrl=${targetUrl}' />" method='POST'>

    <table>
      <tr>
        <td><label><strong><spring:message code="login.label.user" /></strong></label></td>
        <td><input type='text' name='account_name'></td>
      </tr>
      <tr>
        <td><strong><spring:message code="login.label.password" /></strong></td>
        <td><input type='password' name='password'/></td>
      </tr>

      <!-- if this is login for update, ignore remember me check -->
      <c:if test="${empty loginUpdate}">
        <tr>
          <td></td>
          <td>Remember Me: <input type="checkbox" name="remember-me"/></td>
        </tr>
      </c:if>

      <tr>
        <td colspan='2'><input class="btn btn-primary" name="submit" type="submit" value="submit"/></td>
		<td><a type="button" style="margin-left: -125px;" class="btn btn-primary"
             href="<c:url value="/add-account"/>">Create account</a></td>
      </tr>

    </table>

    <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>

  </form>

</div>

</body>
</html>