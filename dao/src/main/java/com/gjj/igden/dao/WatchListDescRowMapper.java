package com.gjj.igden.dao;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.gjj.igden.model.Account;
import com.gjj.igden.model.IWatchListDesc;
import com.gjj.igden.model.WatchListDesc;
import com.gjj.igden.model.WatchListDescKey;

public class WatchListDescRowMapper implements RowMapper<IWatchListDesc> {

	public WatchListDesc mapRow(ResultSet resultSet, int i) throws SQLException {
		WatchListDesc watchListDesc = new WatchListDesc();
		WatchListDescKey watchListDescKey = new WatchListDescKey();
		watchListDescKey.setWatchListId(resultSet.getLong("data_set_id"));
		watchListDescKey.setAccountId(resultSet.getLong("account_fk_id"));
		
		watchListDesc.setWatchListDescKey(watchListDescKey);
		watchListDesc.setWatchListName(resultSet.getString("data_set_name"));
		watchListDesc.setWatchListDetails(resultSet.getString("data_set_description"));
		watchListDesc.setMarketDataFrequency(resultSet.getLong("market_data_frequency"));
		watchListDesc.setDataProviders(resultSet.getString("data_providers"));
		return watchListDesc;
	}
}
