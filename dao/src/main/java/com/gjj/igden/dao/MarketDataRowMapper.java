package com.gjj.igden.dao;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

public class MarketDataRowMapper implements RowMapper<String> {
  public String mapRow(ResultSet resultSet, int i) throws SQLException {
    return resultSet.getString("instId_fk");
  }
}
