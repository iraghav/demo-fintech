package com.gjj.igden.dao.daoimpl;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.stereotype.Repository;

import com.gjj.igden.dao.AbstractDAO;
import com.gjj.igden.dao.daoUtil.DAOException;
import com.gjj.igden.model.Account;
import com.gjj.igden.model.IWatchListDesc;
import com.gjj.igden.model.WatchListDesc;
import com.gjj.igden.model.WatchListDescKey;

@Repository
@Transactional
@SuppressWarnings("unchecked")
public class WatchListDescDaoImpl extends AbstractDAO<IWatchListDesc> {

	public WatchListDesc read(WatchListDesc obj) {
		WatchListDesc wl = (WatchListDesc) em.createQuery("FROM WatchListDesc wl where wl.watchListDescKey.watchListId ="+obj.getWatchListDescKey().getWatchListId()).getSingleResult();
		System.out.println("WatchListDescDaoImpl.read():::::"+wl.getWatchListId());
		return wl;
	}
	
	public WatchListDesc read(Long id, Long accId) {
		/*WatchListDesc watchListDesc = (WatchListDesc) em.createQuery("FROM WatchListDesc wl where"
								+ " wl.watchListDescKey.watchListId="+id).getSingleResult();
		return watchListDesc;*/
		
		WatchListDescKey key = new WatchListDescKey();
		key.setWatchListId(id);
		key.setAccountId(accId);
		return em.find(WatchListDesc.class, key);
	}
	
	public WatchListDesc read(Long id) {
		WatchListDesc watchListDesc = (WatchListDesc) em.createQuery("FROM WatchListDesc wl where"
				+ " wl.watchListDescKey.watchListId="+id).getSingleResult();
		return watchListDesc;
	}
	
	public boolean deleteWatchList(WatchListDesc obj) throws DAOException {
		em.createQuery("DELETE FROM WatchListDesc wl WHERE "
				+ "wl.watchListDescKey.watchListId ="
				+obj.getWatchListDescKey().getWatchListId()).executeUpdate();
		return true;
	}
	
	public WatchListDesc readByWatchListIdAndAccountId(Long watchListId, Long accountId) {
		WatchListDesc wl = (WatchListDesc) em.createQuery("FROM WatchListDesc wl where wl.watchListDescKey.watchListId ="
							+watchListId+" AND wl.watchListDescKey.accountId = "+accountId).getSingleResult();
		System.out.println("WatchListDescDaoImpl.readByWatchListIdAndAccountId():::"+wl.getWatchListId());
		return wl;
	}

	@Override
	public List<IWatchListDesc> readAll() {
		return em.createQuery("FROM WatchListDesc").getResultList();
	}
	
	public boolean createWatchListDesc(WatchListDesc watchListDesc) throws DAOException {
		Long max = getMaxIdFromDatabase();
		watchListDesc.getWatchListDescKey().setWatchListId(max+1);
		super.save(watchListDesc);
		return true;
	}
	
	public List<WatchListDesc> getDataSetsAttachedToAcc(Long id) {
		return em.createQuery("FROM WatchListDesc WHERE account_fk_id = "+id).getResultList();
	}

	public IWatchListDesc getWatchListDesc(Long dsId, Long accId) {
		return (IWatchListDesc) em.createQuery("FROM WatchListDesc WHERE id = "+dsId+" AND Account = "+new Account(accId)).getSingleResult();
	}
	
	public boolean updateWatchListDesc(IWatchListDesc watchListDesc) {
		super.update(watchListDesc);
		return false;
	}
	
	public Long getMaxIdFromDatabase() {
		Long max = (Long) em.createQuery("select MAX(wl.watchListDescKey.watchListId) FROM WatchListDesc wl").getSingleResult();
		System.out.println("WatchListDescDaoImpl.getMaxIdFromDatabase()"+max);
		return null == max ? 0 : max;
	}

	@Override
	public IWatchListDesc read(IWatchListDesc obj) {
		return null;
	}
	

}
