package com.gjj.igden.dao;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.gjj.igden.model.Bar;
import com.gjj.igden.model.BarKey;

public class BarRowMapper implements RowMapper<Bar> {

	@Override
	public Bar mapRow(ResultSet resultSet, int arg1) throws SQLException {
		// TODO Auto-generated method stub
		Bar bar = new Bar();
		BarKey barKey = new BarKey();
		
		barKey.setMdId(resultSet.getLong("md_id"));
		barKey.setInstId(resultSet.getString("instId_fk"));
		bar.setDateTime(resultSet.getDate("date"));
		bar.setOpen(resultSet.getDouble("open"));
		bar.setHigh(resultSet.getDouble("high"));
		bar.setLow(resultSet.getDouble("low"));
		bar.setClose(resultSet.getDouble("close"));
		bar.setVolume(resultSet.getInt("vol"));
		bar.setLogInfo(resultSet.getString("additional_info"));
		bar.setBarKey(barKey);
		return bar;
	}
}
