package com.gjj.igden.dao.test;

import com.gjj.igden.dao.daoimpl.BarDaoImpl;
import com.gjj.igden.model.Bar;
import com.gjj.igden.model.BarKey;

import org.junit.Assert;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.Date;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {JPATestConfig.class})
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class Test03_BarDaoTest {

  private static final Logger logger = LoggerFactory.getLogger(Test03_BarDaoTest.class);

  private static final String AAPL_NASDAQ = "AAPL"+new Date().getTime()+"@NASDAQ";
  
  @Autowired
  private BarDaoImpl barDaoImpl;

  
  @Test
  public void test01_create() throws Exception {
    Bar bar = new Bar();
    bar.setBarSize(10);
    bar.setDateTime(new Date());
    bar.setHigh(50);
    bar.setLogInfo("log info");
    bar.setLow(10);
    bar.setOpen(15);
    bar.setClose(20);
    bar.setBarKey(new BarKey(1L, AAPL_NASDAQ));
        
    Assert.assertTrue(barDaoImpl.createBar(bar));
    
    Set<Bar> barList = barDaoImpl.getBarList(AAPL_NASDAQ);
    logger.debug("Test03_BarDaoTest::Barlist after creating bar::" +bar + " is " + barList);
    Set<Bar> newBar = barList.stream()
					      .filter(p -> Objects.equals(p.getHigh(), 50D))
					      .collect(Collectors.toSet());
    Set<Bar> newBar_id = barList.stream()
      .filter(p -> Objects.equals(p.getId(), 1L))
      .collect(Collectors.toSet());
    Assert.assertEquals(1, newBar.size());
    Assert.assertEquals(1, newBar_id.size());
  }
  
  @Test
  public void test05_update() throws Exception{
    Bar bar = barDaoImpl.getSingleBar(new BarKey(1L,AAPL_NASDAQ));
    logger.debug("Test03_BarDaoTest::Bar before update::" + bar);
    bar.setLogInfo("test update");
    barDaoImpl.updateBar(bar);
    logger.debug("Test03_BarDaoTest::Bar after update::" + barDaoImpl.getSingleBar(new BarKey(1L,AAPL_NASDAQ)));
    final String additionalInfo = barDaoImpl.getSingleBar(new BarKey(1L,AAPL_NASDAQ)).getLogInfo();
    Assert.assertEquals("test update", additionalInfo);
  }

    @Test
  public void test02_testGetSingleBar() {
    Bar bar = barDaoImpl.getSingleBar(new BarKey(1L,AAPL_NASDAQ));
      logger.debug("Test03_BarDaoTest::Get single bar::" + bar);
    Assert.assertNotNull(bar);
  }
    
  @Test
  public void test03_getBarBySymbol() {
    Set<Bar> bars = barDaoImpl.getBarList(AAPL_NASDAQ);
    logger.debug("Test03_BarDaoTest::Bars for symbol " + AAPL_NASDAQ  + "are::" + bars);
    Assert.assertNotNull(bars);
  }

  @Test
  public void test04_search() {
    List<String> bars = barDaoImpl.searchTickersByChars( "AAP");
    logger.debug("Test03_BarDaoTest::Bars for characters 'AAP' are::" + bars);
    Assert.assertNotNull(bars);
  }

  
  @Test
  public void test06_delete() throws Exception {
    Bar bar = barDaoImpl.getSingleBar(new BarKey(1L,AAPL_NASDAQ));
    Assert.assertNotNull(bar);
    Set<Bar> barList = barDaoImpl.getBarList( AAPL_NASDAQ);
    logger.debug("Test03_BarDaoTest::Bars for symbol " + AAPL_NASDAQ + " before delete are::" + barList);
    Set<Bar> result = barList.stream().filter(p -> Objects.equals(p.getHigh(), 50D)).collect(Collectors.toSet());
    Assert.assertEquals(1, result.size());
    Assert.assertTrue(barDaoImpl.deleteBar(1L,  AAPL_NASDAQ));
    barList = barDaoImpl.getBarList( AAPL_NASDAQ);
    logger.debug("Test03_BarDaoTest::Bars for symbol symbol " + AAPL_NASDAQ + " after delete are::" + barList);
    result = barList.stream().filter(p -> Objects.equals(p.getHigh(), 50)).collect(Collectors.toSet());
    Assert.assertEquals(0, result.size());
  }

}
