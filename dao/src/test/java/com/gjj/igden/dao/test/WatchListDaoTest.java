package com.gjj.igden.dao.test;

import com.gjj.igden.dao.WatchListDescDao;
import com.gjj.igden.model.IWatchListDesc;
import com.gjj.igden.model.WatchListDesc;

import org.junit.Assert;
import org.junit.Before;
import org.junit.FixMethodOrder;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabase;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseBuilder;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.List;



@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:beans-cp.xml"})
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class WatchListDaoTest {
  @Autowired
  private WatchListDescDao watchListDescDao;

  @Before
  public void setUp() {
    EmbeddedDatabase db = new EmbeddedDatabaseBuilder()
      .setType(EmbeddedDatabaseType.H2)
      .addScript("db-init-sql-script/init-db-fintech_wsH2_moreData.sql")
      .build();
    NamedParameterJdbcTemplate template = new NamedParameterJdbcTemplate(db);
    watchListDescDao.setNamedParamJbd(template);
  }

  @Test
  public void test02_AddTicker() {
    boolean resultFlag = watchListDescDao.addTicker(15L, "C@NASDAQ");
    Assert.assertTrue(resultFlag);
    Assert.assertEquals("C@NASDAQ", watchListDescDao.getAllStockSymbols(15L).get(2));
  }

  @Test
  public void test01_GetAllStockSymbols() {
    List<String> tickerList = watchListDescDao.getAllStockSymbols(2L);
    final int expectedDataSetsAmount = 18;
    System.out.println(tickerList);
    Assert.assertEquals(expectedDataSetsAmount, tickerList.size());
  }

  @Test
  public void test03_Read() throws Exception {
    List<IWatchListDesc> watchListDescs = watchListDescDao.getDataSetsAttachedToAcc(1L);
    for(IWatchListDesc watchListDesc : watchListDescs) {
		WatchListDesc desc = (WatchListDesc) watchListDesc;
		List<String> stockList = watchListDescDao.getAllStockSymbols(watchListDesc.getWatchListId());
		desc.setStockSymbolsList(stockList);
	}
    
    final int stockSymbolNumAttachedToWatchedList17th = 18;
    Assert.assertEquals(stockSymbolNumAttachedToWatchedList17th,
    watchListDescs.get(1).getStockSymbolsList().size());
  }

  @Test
  public void test04_GetDataSetsAttachedToAcc() {
    List<IWatchListDesc> dataSetList = watchListDescDao.getDataSetsAttachedToAcc(1L);
    final int expectedDataSetsAmount = 9;
    Assert.assertEquals(expectedDataSetsAmount, dataSetList.size());
  }

  @Test
  public void test05_ReturnBarList() {
    IWatchListDesc dataSet = watchListDescDao.getWatchListDesc(1L, 1L);
    System.out.println("In return bar list" +dataSet.getWatchListName());
    Assert.assertNotNull(dataSet);
    Assert.assertEquals("test-aapl-5minBar-preMarketdata", dataSet.getWatchListName());
  }

  @Test
  public void test08_Delete() throws Exception {
    List<IWatchListDesc> dataSetList = watchListDescDao.getDataSetsAttachedToAcc(1L);
    final int expectedDataSetsAmount = 9;
    System.out.println(" again ");
    dataSetList.forEach(p -> System.out.println(p.getId()));
    Assert.assertEquals(expectedDataSetsAmount, dataSetList.size());
    boolean deleteResultFlag = watchListDescDao.deleteWatchListDesc(dataSetList.get(0));
    //		Assert.assertTrue(deleteResultFlag);
    System.out.println("after deletion ");
    dataSetList = watchListDescDao.getDataSetsAttachedToAcc(1L);
    final int expectedDataSetsAmountAfterDeletion = 8;
    dataSetList.forEach(p -> System.out.println(p.getId()));
    Assert.assertEquals(expectedDataSetsAmountAfterDeletion, dataSetList.size());
  }

  @Test
  public void test07_CreateDataSet() throws Exception {
    IWatchListDesc dataSet = watchListDescDao.getWatchListDesc(1L, 1L);
    List<IWatchListDesc> dataSetList = watchListDescDao.getDataSetsAttachedToAcc(1L);
    dataSetList.forEach(p -> System.out.print(p.getId() + " ; "));
    int expectedDataSetsAmountAfterDeletion = 9;
    Assert.assertEquals(expectedDataSetsAmountAfterDeletion, dataSetList.size());
    Assert.assertNotNull(dataSet);
    //dataSet.setWatchListId(111);
    dataSet.setWatchListName("just testing around");
    watchListDescDao.createWatchListDesc(dataSet);
  }

  @Test
  public void test06_UpdateDesc() throws Exception {
    IWatchListDesc dataSet = watchListDescDao.getWatchListDesc(1L, 1L);
    dataSet.setWatchListName("test update");
    watchListDescDao.updateWatchListDesc(dataSet);
    final String dataSetNameDirect = watchListDescDao.getWatchListDesc(1L, 1L).getWatchListName();
    Assert.assertEquals("test update", dataSetNameDirect);
  }
}