package com.gjj.igden.dao.test;

import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.apache.commons.dbcp.BasicDataSource;
import org.junit.Assert;
import org.junit.Before;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabase;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseBuilder;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.gjj.igden.dao.AccountDao;
import com.gjj.igden.model.Account;

@SuppressWarnings("Duplicates")
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {JPATestConfig.class})
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class AccountDaoImplTest {

    @Autowired
    private AccountDao testAccountDaoImpl;
    
    @Autowired
    BasicDataSource dataSource;
    

    @Before
    public void setUpInMemoryDataBaseForTests() {
        EmbeddedDatabase db = new EmbeddedDatabaseBuilder()
                .setType(EmbeddedDatabaseType.H2) // toask: is this line  redundant  ?
                .addScript("db-init-sql-script/init-db-fintech_wsH2_moreData.sql")
                .build();
        NamedParameterJdbcTemplate template = new NamedParameterJdbcTemplate(db);
        testAccountDaoImpl.setNamedParamJbd(template);
    }

    @Test
    public void test01_CreateH2DataBaseTest() {
        List<Account> accounts = testAccountDaoImpl.getAllAccounts();
        Assert.assertNotNull(accounts);
    }

    @Test
    public void test02_ReadAll() throws Exception {
        List<Account> accounts = testAccountDaoImpl.getAllAccounts();
        Assert.assertEquals(2, accounts.size());
    }

    @Test
    public void test03_ReadById() throws Exception {
        Account account = testAccountDaoImpl.getAccountById(1L);
        Assert.assertNotNull(account);
    }

    @Test
    public void test06_Delete() throws Exception {
        List<Account> accounts = testAccountDaoImpl.getAllAccounts();
        int originalAmount = accounts.size();
        testAccountDaoImpl.delete(accounts.get(0).getId());
        accounts = testAccountDaoImpl.getAllAccounts();
        int afterDel = accounts.size();
        Assert.assertEquals(originalAmount, afterDel + 1);
    }

    @Test
    public void test04_Update() throws Exception {
        Account accounts = testAccountDaoImpl.getAccountById(1L);
        String oldInfo = accounts.getAdditionalInfo();
        accounts.setAdditionalInfo("test update");
        testAccountDaoImpl.update(accounts);
        final String additionalInfo = testAccountDaoImpl.getAccountById(1L).getAdditionalInfo();
        Assert.assertNotEquals(oldInfo, additionalInfo);
        Assert.assertEquals("test update", additionalInfo);
    }

    @Test
    public void test05_CreateAccount() throws Exception {
        Account account = new Account();
        account.setAccountName("test");
        account.setEmail("test@test");
        account.setAdditionalInfo("test my test");
        account.setPassword("1");
        account.setAccountName("test");
        boolean flag = testAccountDaoImpl.create(account);
        System.out.println(flag);
        List<Account> accounts = testAccountDaoImpl.getAllAccounts();
        Stream<Account> newAccount = accounts.stream().filter(p -> Objects.equals(p.getAccountName(),
                "test"));
        List<Account> result = newAccount.collect(Collectors.toList());
        Stream<Account> allAccounts = accounts.stream().filter(p -> p.getId() > 0);
        List<Account> testResult = allAccounts.collect(Collectors.toList());
        Assert.assertEquals(1, result.size());
        Assert.assertEquals("test@test", result.get(0).getEmail());
        Assert.assertEquals(3, testResult.size());
    }
}
