package com.gjj.igden.dao.test.impl;


import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.apache.commons.lang3.StringUtils;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcDaoSupport;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import com.gjj.igden.dao.BarDao;
import com.gjj.igden.dao.BarRowMapper;
import com.gjj.igden.dao.daoUtil.DAOException;
import com.gjj.igden.model.Bar;
import com.gjj.igden.model.BarKey;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Maps;

@Service
public class BarDaoServiceImpl implements BarDao {
  
  
  NamedParameterJdbcTemplate jdbcTemplate;

   private static Bar createBar(BarKey barKey) {
	   Bar bar = new Bar();
	   bar.setBarKey(barKey);
	   return bar;
   }

  @Override
  public Bar getSingleBar(long mdId, String instId) {
	 String sql = "select * from market_data where md_id = :id and instId_fk = :instId LIMIT 1";
	 MapSqlParameterSource sqlParams = new MapSqlParameterSource();
	 sqlParams.addValue("id", mdId);
	 sqlParams.addValue("instId", instId);
	 Bar bar = jdbcTemplate.queryForObject(sql, sqlParams, new BarRowMapper());
    return  bar; 
  }

  @Override
  public List<Bar> getBarList(String instId) {
		String sql = "select * from market_data where instId_fk = :instId";
		MapSqlParameterSource sqlParams = new MapSqlParameterSource();
		sqlParams.addValue("instId", instId);
		List<Bar>  barlist = jdbcTemplate.query(sql, sqlParams,new BarRowMapper());
		return barlist;
  }

  @Override
  public boolean updateBar(Bar bar) {
	  
	  String sql = "update market_data set additional_info = :info where md_id = :id and instId_fk = :instId";
	  MapSqlParameterSource sqlParams = new MapSqlParameterSource();
	  sqlParams.addValue("id", bar.getId());
	  sqlParams.addValue("instId", bar.getBarKey().getInstId());
	  sqlParams.addValue("info", bar.getLogInfo());
	  int i = jdbcTemplate.update(sql, sqlParams);
	  return getSingleBar(bar.getId(), bar.getBarKey().getInstId().toString()).equals(bar);
  }

  @Override
  public boolean createBar(Bar bar) throws DAOException {
	  
	  String sql = "insert into market_data (`md_id`, `instId_fk`, `ticker`, `date`, `open`, `high`, `low`, `close`, `vol`, `additional_info`) values "
	  		+ "(:id, :instId, :ticker, :date, :open, :high, :low, :close, :vol, :info)";
	  
	  MapSqlParameterSource sqlParams = new MapSqlParameterSource();
	  sqlParams.addValue("id", bar.getId());
	  sqlParams.addValue("instId", bar.getBarKey().getInstId());
	  sqlParams.addValue("ticker", StringUtils.substringBefore(bar.getBarKey().getInstId(), "@"));
	  sqlParams.addValue("date", bar.getDateTime());
	  sqlParams.addValue("open", bar.getOpen());
	  sqlParams.addValue("high", bar.getHigh());
	  sqlParams.addValue("low", bar.getLow());
	  sqlParams.addValue("close", bar.getClose());
	  sqlParams.addValue("vol", bar.getVolume());
	  sqlParams.addValue("info", bar.getLogInfo());
	  
	  int i = jdbcTemplate.update(sql, sqlParams);
	  
	  return i > 0;
  }

  @Override
  public boolean deleteBar(long mdId, String instId) {
		String sql = "delete from market_data where md_id = :id and instId_fk = :instId";
		MapSqlParameterSource sqlParams = new MapSqlParameterSource();
		sqlParams.addValue("id", mdId);
		sqlParams.addValue("instId", instId);
		int i = jdbcTemplate.update(sql, sqlParams);
		return i > 0;
  }

  @Override
  public boolean deleteBar(Bar bar) {
    return deleteBar(bar.getId(), bar.getBarKey().getInstId().toString());
  }

  @Override
  public List<String> searchTickersByChars(String tickerNamePart) {
		String sql = "select ticker from market_data where ticker LIKE '%" + ":tickerNamePart" + "%'";
		MapSqlParameterSource sqlParams = new MapSqlParameterSource();
		sqlParams.addValue("tickerNamePart", tickerNamePart);
		List<String> tickersByChars = jdbcTemplate.queryForList(sql, sqlParams, String.class);
	  return tickersByChars;
  }

  @Override
  public void setNamedParamJbd(NamedParameterJdbcTemplate namedParamJbd) {
	  this.jdbcTemplate = namedParamJbd;
  }
}
