package com.gjj.igden.dao.test.impl;

import java.util.List;

import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Service;

import com.gjj.igden.dao.WatchListDescDao;
import com.gjj.igden.dao.WatchListDescRowMapper;
import com.gjj.igden.model.IWatchListDesc;
import com.gjj.igden.model.WatchListDesc;

@Service
public class WatchListDescDaoServiceImpl implements WatchListDescDao {

	NamedParameterJdbcTemplate jdbcTemplate;

	@Override
	public List<String> getAllStockSymbols(Long id) {
		//data_set
		String sql = "select instId from wl_tickers where watchlist_id_fk = :id";
		
		MapSqlParameterSource sqlParams = new MapSqlParameterSource();
		sqlParams.addValue("id", id.intValue());
		List<String> stockList = jdbcTemplate.queryForList(sql, sqlParams, String.class);
		return stockList;
	}

	@Override
	public List<IWatchListDesc> getDataSetsAttachedToAcc(Long id) {
		String sql = "select * from Data_Set where account_fk_id = :id";
		MapSqlParameterSource params = new MapSqlParameterSource("id", id.intValue());
		List<IWatchListDesc>  watchListDescList = jdbcTemplate.query(sql, params , new WatchListDescRowMapper());
		
		return watchListDescList;
	}

	@Override
	public void setNamedParamJbd(NamedParameterJdbcTemplate namedParamJbd) {
		this.jdbcTemplate = namedParamJbd;
	}

	@Override
	public IWatchListDesc getWatchListDesc(Long dsId, Long accId) {
		String sql = "select * from Data_Set where account_fk_id = :id and data_set_id =:dsId";
		MapSqlParameterSource sqlParameterSource = new MapSqlParameterSource();
		sqlParameterSource.addValue("id", accId.intValue());
		sqlParameterSource.addValue("dsId", dsId);
		List<IWatchListDesc>  watchListDescList = jdbcTemplate.query(sql, sqlParameterSource, new WatchListDescRowMapper());
		return watchListDescList.get(0);
	}

	@Override
	public boolean addTicker(Long watchlistId, String tickerName) {
		String sql = "INSERT INTO `wl_tickers` (`instId`, `watchlist_id_fk`) values ( :tickerName, :id)";
		MapSqlParameterSource sqlParams = new MapSqlParameterSource();
		sqlParams.addValue("tickerName", tickerName);
		sqlParams.addValue("id", watchlistId.intValue());
		
		int i = jdbcTemplate.update(sql, sqlParams);
		return i > 0;
	}

	@Override
	public boolean deleteWatchListDesc(Long dsId, Long accId) {
		String sql = "delete from Data_Set where account_fk_id = :id and data_set_id =:dsId";
		MapSqlParameterSource sqlParameterSource = new MapSqlParameterSource();
		sqlParameterSource.addValue("id", accId.intValue());
		sqlParameterSource.addValue("dsId", dsId); 
		int i  = jdbcTemplate.update(sql, sqlParameterSource);
		return i > 0;
	}

	@Override
	public boolean deleteWatchListDesc(IWatchListDesc watchListDesc) {
		return deleteWatchListDesc(watchListDesc.getWatchListDescKey().getWatchListId(), watchListDesc.getWatchListDescKey().getAccountId());
	}

	@Override
	public boolean createWatchListDesc(IWatchListDesc watchListDesc) {
		return false;
	}

	@Override
	public boolean updateWatchListDesc(IWatchListDesc watchListDesc) {
		String sql = "update Data_Set set data_set_name= :name where account_fk_id = :id and data_set_id =:dsId";
		MapSqlParameterSource sqlParameterSource = new MapSqlParameterSource();
		sqlParameterSource.addValue("id", watchListDesc.getWatchListDescKey().getAccountId().intValue());
		sqlParameterSource.addValue("dsId", watchListDesc.getWatchListDescKey().getWatchListId().intValue());
		sqlParameterSource.addValue("name", watchListDesc.getWatchListName());
		int i = jdbcTemplate.update(sql, sqlParameterSource); 
		return i > 0;
	}
}
