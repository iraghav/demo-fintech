package com.gjj.igden.dao.test.impl;

import java.io.InputStream;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import javax.sql.DataSource;

import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.stereotype.Service;

import com.gjj.igden.dao.AccountDao;
import com.gjj.igden.dao.AccountRowMapper;
import com.gjj.igden.model.Account;
import com.gjj.igden.model.WatchListDesc;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Maps;

@Service
public class AccountDaoServiceImpl implements AccountDao {
  
  NamedParameterJdbcTemplate jdbcTemplate;

   public void setDataSource(DataSource dataSource) {
  }

  public void setNamedParamJbd(NamedParameterJdbcTemplate namedParamJbd) {
	  this.jdbcTemplate = namedParamJbd;
  }

  public List<Account> getAllAccounts() {
   	  String sql = "select * from Account";
	  List<Account> accountList = jdbcTemplate.query(sql, new AccountRowMapper());
	  
	  return accountList.size() > 0 ? accountList : null;
  }

  public boolean delete(Account account) {
    return false;
  }

  public boolean delete(Long id) {
	String sql = "delete from Account where account_id = :accId";
	SqlParameterSource namedParameters = new MapSqlParameterSource("accId", id.intValue());
	int i = jdbcTemplate.update(sql, namedParameters);
	return i > 0;
  }

  public Account getAccountById(Long id) {
	String sql = "select * from Account where account_id = :accId";
	SqlParameterSource namedParameters = new MapSqlParameterSource("accId", id.intValue());
	return jdbcTemplate.queryForObject(sql, namedParameters, new AccountRowMapper());
  }

  public boolean update(Account acc) {
	  String sql = "update Account set additional_info= :info where account_id = :accId";
	  MapSqlParameterSource namedParameters = new MapSqlParameterSource();
	  namedParameters.addValue("info",acc.getAdditionalInfo());
	  namedParameters.addValue("accId", acc.getId().intValue());
	  int i = jdbcTemplate.update(sql, namedParameters);
	  return i > 0;
  }

  public boolean create(Account account) {
	  
	 String sql = "insert into Account (account_name,email,additional_info,password) values (:name, :email, :info,:pwd)";
	 MapSqlParameterSource namedParameters = new MapSqlParameterSource();
	 namedParameters.addValue("name",account.getAccountName());
	 namedParameters.addValue("email", account.getEmail());
	 namedParameters.addValue("info", account.getAdditionalInfo());
	 namedParameters.addValue("pwd", account.getPassword());
	 
	 int i = jdbcTemplate.update(sql, namedParameters);
	 return i > 0;
	  
	  
	 
  }

  @Override
  public boolean setImage(Long accId, InputStream is) {
    return true;
  }

  @Override
  public byte[] getImage(Long accId) {
    return new byte[0];
  }
}