package com.gjj.igden.dao.test;

import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

import org.junit.Assert;
import org.junit.Before;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabase;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseBuilder;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.gjj.igden.dao.BarDao;
import com.gjj.igden.model.Bar;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:beans-cp.xml"})
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class BarDaoTest {
  @Autowired
  private BarDao barDaoImpl;

  @Before
  public void setUp() {
    EmbeddedDatabase db = new EmbeddedDatabaseBuilder()
      .setType(EmbeddedDatabaseType.H2)
      .addScript("db-init-sql-script/init-db-fintech_wsH2_moreData.sql")
      .build();
    NamedParameterJdbcTemplate template = new NamedParameterJdbcTemplate(db);
    barDaoImpl.setNamedParamJbd(template);
  }

  @Test
  public void test01_GetSingleBar() {
    Bar bar = barDaoImpl.getSingleBar(1, "AAPL@NASDAQ");
    Assert.assertNotNull(bar);
  }

  @Test
  public void test02_ReturnBarList() {
    List<Bar> bars = barDaoImpl.getBarList( "AAPL@NASDAQ");
    System.out.println(bars);
    Assert.assertNotNull(bars);
  }

  @Test
  public void test03_Search() {
    List<String> bars = barDaoImpl.searchTickersByChars("AAP");
    System.out.println(bars);
    Assert.assertNotNull(bars);
  }



  @Test
  public void test05_CreateBar() throws Exception {
    Bar bar = barDaoImpl.getSingleBar(1,  "AAPL@NASDAQ");
    System.out.println("Inside Create" + bar);
    Assert.assertNotNull(bar);
    bar.setId((long) 111);
    System.out.println("Inside Create111" + bar);
    boolean flag = barDaoImpl.createBar(bar);
    System.out.println("Created " + flag);
    Assert.assertTrue(flag);
    List<Bar> barList = barDaoImpl.getBarList("AAPL@NASDAQ");
    System.out.println("--------------Updateed Size" + barList.size());
    System.out.println("Updated BarList and size" + barList);
    System.out.println("--------------");
    
    List<Bar> newBar = barList.stream()
      .filter(p -> Objects.equals(p.getHigh(), 296.25))
      .collect(Collectors.toList());
    System.out.println("new bar size" + newBar.size());
    List<Bar> newBar_id = barList.stream()
      .filter(p -> Objects.equals(p.getId(), 111L))
      .collect(Collectors.toList());
    System.out.println("new bar id size" + newBar_id.size());
    System.out.println("new Bar details" +newBar);
    Assert.assertEquals(3, newBar.size());
    Assert.assertEquals(1, newBar_id.size());
  }

  @Test
  public void test04_Delete() throws Exception {
    Bar bar = barDaoImpl.getSingleBar((long)1,  "AAPL@NASDAQ");
    System.out.println(bar);
    Assert.assertNotNull(bar);
    bar.setId((long) 111);
    System.out.println(bar);
    Assert.assertTrue(barDaoImpl.createBar(bar));
    List<Bar> barList = barDaoImpl.getBarList("AAPL@NASDAQ");
    List<Bar> result = barList.stream().filter(p -> Objects.equals(p.getHigh(), 296.25))
      .collect(Collectors.toList());
    System.out.println(result);
    Assert.assertEquals(3, result.size());
    Assert.assertTrue(barDaoImpl.deleteBar(111L,  "AAPL@NASDAQ"));
    barList = barDaoImpl.getBarList("AAPL@NASDAQ");
    result = barList.stream().filter(p -> Objects.equals(p.getHigh(), 296.25)).collect(Collectors.toList());
    Assert.assertEquals(2, result.size());
  }

  @Test
  public void test06_Update() throws Exception {
    Bar bar = barDaoImpl.getSingleBar(1,  "AAPL@NASDAQ");
    System.out.println("Update" + bar);
    bar.setLogInfo("test update");
    barDaoImpl.updateBar(bar);
    final String additionalInfo = barDaoImpl.getSingleBar(1,  "AAPL@NASDAQ").getLogInfo();
    Assert.assertEquals("test update", additionalInfo);
  }
}
