package com.gjj.igden.model;

import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import org.javatuples.Ennead;

import com.gjj.igden.utils.EntityId;
import com.google.common.base.Objects;

@Entity
@Table(name="market_data")
public class Bar implements InterfaceOHLCData, EntityId {

	@EmbeddedId
	public BarKey barKey;
	
	@Transient
    protected int barSize;
    protected double open;
    protected double high;
    protected double low;
    protected double close;
    @Column(name="vol")
    protected long volume;
    @Column(name="additional_info")
    protected String logInfo;
    @Column(name="date")
    @Temporal(TemporalType.TIMESTAMP)
    protected Date dateTime;

    public Bar() {
    }

    public Bar(WatchListDesc dataSet, int barSize) {
        this.barSize = barSize;
    }


    public Bar(Bar bar) {
        this.barKey.mdId = bar.barKey.mdId;
        this.barSize = bar.barSize;
        this.open = bar.open;
        this.high = bar.high;
        this.low = bar.low;
        this.close = bar.close;
        this.volume = bar.volume;
        this.logInfo = bar.logInfo;
    }

    public Bar(InstId instId, long dateTime, Long mdId, WatchListDesc dataSet, int barSize, double open,
               double high, double low, double close, long volume, String logInfo) {
        this.barKey.mdId = mdId;
        this.barSize = barSize;
        this.open = open;
        this.high = high;
        this.low = low;
        this.close = close;
        this.volume = volume;
        this.logInfo = logInfo;
    }

    public Bar(
            InstId instId,
            int barSize,
            long dateTime,
            double open,
            double high,
            double low,
            double close) {
        this(instId, barSize, dateTime, open, high, low, close, 0, 0, null);
    }

    public Bar(
            InstId instId,
            int barSize,
            long dateTime,
            double open,
            double high,
            double low,
            double close,
            long volume,
            String logInfo) {
        this(instId, barSize, dateTime, open, high, low, close, volume, 0, logInfo);
    }

    public Bar(
            InstId instId,
            int barSize,
            long dateTime,
            double open,
            double high,
            double low,
            double close,
            long volume,
            long mdId,
            String logInfo) {
        this.barKey.mdId = mdId;
        this.barSize = barSize;
        this.open = open;
        this.high = high;
        this.low = low;
        this.close = close;
        this.volume = volume;
        this.logInfo = logInfo;
    }

    public Bar(Ennead<InstId, Long, Long, Integer, Double, Double,
            Double, Double, Long> ennead) {
        setMainData(ennead);
    }

    public Bar(IMetaDataUnit resultSet) {
        setMainData(resultSet.getDataUnit());
    }

    public Long getMdId() {
        return barKey.mdId;
    }

    public void setMdId(Long mdId) {
    	this.barKey.mdId = mdId;
    }

    public int getBarSize() {
        return barSize;
    }

    public void setBarSize(int barSize) {
        this.barSize = barSize;
    }

    public double getOpen() {
        return open;
    }

    public void setOpen(double open) {
        this.open = open;
    }

    public double getHigh() {
        return high;
    }

    public void setHigh(double high) {
        this.high = high;
    }

    public double getLow() {
        return low;
    }

    public void setLow(double low) {
        this.low = low;
    }

    public double getClose() {
        return close;
    }

    public void setClose(double close) {
        this.close = close;
    }

    public long getVolume() {
        return volume;
    }

    public void setVolume(long volume) {
        this.volume = volume;
    }

    public void setLogInfo(String logInfo) {
        this.logInfo = logInfo;
    }

    public String getLogInfo() {
        return this.logInfo;
    }
    
	public Date getDateTime() {
		return dateTime;
	}

	public void setDateTime(Date dateTime) {
		this.dateTime = dateTime;
	}

    public BarKey getBarKey() {
		return barKey;
	}

	public void setBarKey(BarKey barKey) {
		this.barKey = barKey;
	}

	public String toString() {
        Instant fromUnixTimestamp = Instant.ofEpochSecond(dateTime.getTime());
        LocalDateTime time = LocalDateTime.ofInstant(fromUnixTimestamp,
                ZoneId.of("UTC-4"));
        return "\n{ " +
                "mdId=" + barKey.mdId +
                "; dateTime=" + time +
                "; barSize=" + barSize +
                "; high=" + high +
                "; low=" + low +
                "; open=" + open +
                "; close=" + close +
                "; volume=" + volume +
                "; info=" + logInfo +
                " } ";
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Bar bar = (Bar) o;
        return Objects.equal(dateTime, bar.dateTime) &&
                // TODO m high priority: the line below - in some moment barSize is not setted up
                // TODO m high priority: use test createDataSetTestImportant() to determine the
                // TODO m high priority: bug source
                Objects.equal(barSize, bar.barSize) &&
                Objects.equal(high, bar.high) &&
                Objects.equal(low, bar.low) &&
                Objects.equal(open, bar.open) &&
                Objects.equal(close, bar.close) &&
                Objects.equal(volume, bar.volume) &&
                Objects.equal(barKey.mdId, bar.barKey.mdId);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(dateTime, barSize, high, low, open, close, volume, barKey.mdId);
    }

    public void reset() {
        barSize = -1;
        barKey.mdId = null;
        high = 0.0;
        low = 0.0;
        open = 0.0;
        close = 0.0;
        volume = 0;
        logInfo = null;
    }

    public void copy(Bar bar) {
        this.barKey.mdId = bar.barKey.mdId;
        this.dateTime = bar.dateTime;
        this.barSize = bar.barSize;
        this.high = bar.high;
        this.low = bar.low;
        this.open = bar.open;
        this.close = bar.close;
        this.volume = bar.volume;
        this.logInfo = bar.logInfo;
    }

    @Override
    public Ennead<InstId, Long, Long, Integer, Double, Double, Double, Double, Long> getMainData() {
        return null;
    }

    public void setMainData(Ennead<InstId, Long, Long, Integer, Double, Double,
            Double, Double, Long> ennead) {
        this.barKey.mdId = ennead.getValue1();
        this.dateTime = new Date(ennead.getValue2());
        this.barSize = ennead.getValue3();
        this.high = ennead.getValue4();
        this.low = ennead.getValue5();
        this.open = ennead.getValue6();
        this.close = ennead.getValue7();
        this.volume = ennead.getValue8();
        // this.logInfo = ennead.logInfo;
    }

	@Override
	public Long getId() {
		return barKey.mdId;
	}

	@Override
	public void setId(Long id) {
		this.barKey.mdId = id;
		
	}
}
