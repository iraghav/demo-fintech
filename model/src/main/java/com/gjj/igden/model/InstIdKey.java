package com.gjj.igden.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class InstIdKey implements Serializable {

	private static final long serialVersionUID = -3618095185677608289L;

	@Column(name = "inst_id")
	private String id;
	
	@Column(name = "watchlist_id")
	private Long watchListDescId;
	
	@Column(name = "account_fk_Id")
	private Long accountId;

	public InstIdKey() {
	}

	public InstIdKey(String id) {
		this.id = id;
	}

	public InstIdKey(String symbol, String exchId) {
		this.id = symbol + "@" + exchId;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public Long getWatchListDescId() {
		return watchListDescId;
	}

	public void setWatchListDescId(Long watchListDescId) {
		this.watchListDescId = watchListDescId;
	}

	public Long getAccountId() {
		return accountId;
	}

	public void setAccountId(Long accountId) {
		this.accountId = accountId;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((accountId == null) ? 0 : accountId.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((watchListDescId == null) ? 0 : watchListDescId.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		InstIdKey other = (InstIdKey) obj;
		if (accountId == null) {
			if (other.accountId != null)
				return false;
		} else if (!accountId.equals(other.accountId))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (watchListDescId == null) {
			if (other.watchListDescId != null)
				return false;
		} else if (!watchListDescId.equals(other.watchListDescId))
			return false;
		return true;
	}

	

}