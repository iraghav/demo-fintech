package com.gjj.igden.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MapsId;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.apache.commons.collections4.FactoryUtils;
import org.apache.commons.collections4.list.LazyList;

@Entity
@Table(name = "data_set")
public class WatchListDesc implements IWatchListDesc, Serializable {

	private static final long serialVersionUID = -8926658887608629306L;

	@EmbeddedId
	private WatchListDescKey watchListDescKey;
	
	@ManyToOne
	@MapsId("account_fk_id")
	@JoinColumn(name = "account_fk_id", referencedColumnName = "account_id")
	private Account account;
	
	@Column(name="data_set_name")
	private String watchListName;
	
	@Column(name="data_set_details")
	private String watchListDetails;
	
	@Column(name="market_data_frequency")
	private Long marketDataFrequency;
	
	@Column(name="data_providers")
	private String dataProviders;
	
	@OneToMany(mappedBy = "watchListDesc", cascade = CascadeType.ALL)
	private List<InstId> instIdList;
	
	@Transient
	private List<String> stockSymbolsList;
	
	@Transient
	private List<OperationParameters> operationParameterses = LazyList.lazyList(new ArrayList<>(),
			FactoryUtils.instantiateFactory(OperationParameters.class));
	
	
	public WatchListDesc() {
		watchListDescKey = new WatchListDescKey();
	}

	public WatchListDesc(Account account) {
		this.account = account;
		this.watchListDescKey = new WatchListDescKey(account.getId());
	}
	
	public WatchListDesc(Long watchListId) {
		this.watchListDescKey = new WatchListDescKey();
		this.watchListDescKey.setWatchListId(watchListId);
	}
	

	public List<String> getStockSymbolsList() {
		return stockSymbolsList;
	}

	public void setStockSymbolsList(List<String> stockSymbolsList) {
		this.stockSymbolsList = stockSymbolsList;
	}

	public void setStockSymbolsListFromOperationList(List<OperationParameters> stockSymbolsList) {
		List<String> stringList = stockSymbolsList.stream().map(OperationParameters::getName)
				.collect(Collectors.toList());
		stringList.forEach(System.out::println);
		this.stockSymbolsList = stringList;
	}

	public List<OperationParameters> getOperationParameterses() {
		return operationParameterses;
	}

	public void setOperationParameterses(List<OperationParameters> operationParameterses) {
		this.operationParameterses = operationParameterses;
	}

	public Long getWatchListId() {
		return watchListDescKey.getWatchListId();
	}

	public Account getAccount() {
		return account;
	}

	public void setAccount(Account account) {
		this.account = account;
		this.getWatchListDescKey().setAccountId(account.getId());
	}

	public String getWatchListName() {
		return watchListName;
	}

	public void setWatchListName(String watchListName) {
		this.watchListName = watchListName;
	}

	public String getWatchListDetails() {
		return watchListDetails;
	}

	public void setWatchListDetails(String watchListDetails) {
		this.watchListDetails = watchListDetails;
	}

	public Long getMarketDataFrequency() {
		return marketDataFrequency;
	}

	public void setMarketDataFrequency(Long marketDataFrequency) {
		this.marketDataFrequency = marketDataFrequency;
	}

	public String getDataProviders() {

		// TODO m it should be List<providerID> like phone in social network
		return dataProviders;
	}

	public void setDataProviders(String dataProviders) {
		this.dataProviders = dataProviders;
	}

	public WatchListDescKey getWatchListDescKey() {
		return watchListDescKey;
	}

	public void setWatchListDescKey(WatchListDescKey watchListDescKey) {
		this.watchListDescKey = watchListDescKey;
	}

	@Override
	public String toString() {
		return "WatchListDesc [watchListDescKey=" + watchListDescKey + ", watchListName="
				+ watchListName + ", watchListDetails=" + watchListDetails + ", marketDataFrequency="
				+ marketDataFrequency + ", dataProviders=" + dataProviders + ", stockSymbolsList=" + stockSymbolsList
				+ ", operationParameterses=" + operationParameterses + "]";
	}

	@Transient
	@Override
	public Long getId() {
		return getWatchListId();
	}

	@Override
	public void setId(Long id) {
		this.watchListDescKey.setWatchListId(id);
	}

	@Override
	public void setWatchListId(Long watchListId) {
		this.getWatchListDescKey().setWatchListId(watchListId);
	}

	public List<InstId> getInstIdList() {
		return instIdList;
	}

	public void setInstIdList(List<InstId> instIdList) {
		this.instIdList = instIdList;
	}
	
	public void setInstIdList() {
		this.instIdList = new ArrayList<>();
		this.getOperationParameterses().forEach(op -> {
			InstId instId = new InstId();
			instId.setInstId(op.getName());
			instId.setWatchListDescId(this.getWatchListId());
			instId.setAccountId(this.getAccount().getId());
			System.out.println(instId.getWatchListDescId());
			
			this.instIdList.add(instId);
		});
	}
}

