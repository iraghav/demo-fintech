package com.gjj.igden.xml.model;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "account")
@XmlAccessorType(XmlAccessType.FIELD)
public class Account {
	@XmlAttribute()
	private Long id;
	private String accountName;
	private String email;
	private String additionalInfo;
	private String password;
	private String creationDate;

	public Account() {
	}
	
	public Account(com.gjj.igden.model.Account ac) {
		this.id = ac.getId();
		this.accountName = ac.getAccountName();
		this.email = ac.getEmail();
		this.additionalInfo = ac.getAdditionalInfo();
		this.password = ac.getPassword();
	}

	public Account(Long id, String accountName, String email, String additionalInfo, String password,
			String creationDate) {
		super();
		this.id = id;
		this.accountName = accountName;
		this.email = email;
		this.additionalInfo = additionalInfo;
		this.password = password;
		this.creationDate = creationDate;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getAccountName() {
		return accountName;
	}

	public void setAccountName(String accountName) {
		this.accountName = accountName;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getAdditionalInfo() {
		return additionalInfo;
	}

	public void setAdditionalInfo(String additionalInfo) {
		this.additionalInfo = additionalInfo;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(String creationDate) {
		this.creationDate = creationDate;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Account [id=");
		builder.append(id);
		builder.append(", accountName=");
		builder.append(accountName);
		builder.append(", email=");
		builder.append(email);
		builder.append(", additionalInfo=");
		builder.append(additionalInfo);
		builder.append(", password=");
		builder.append(password);
		builder.append(", creationDate=");
		builder.append(creationDate);
		builder.append("]");
		return builder.toString();
	}

}
