package com.gjj.igden.xml.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.apache.commons.collections4.FactoryUtils;
import org.apache.commons.collections4.list.LazyList;

import com.gjj.igden.model.IWatchListDesc;
import com.gjj.igden.model.InstId;
import com.gjj.igden.model.OperationParameters;
import com.gjj.igden.model.WatchListDescKey;

public class WatchListDesc implements IWatchListDesc, Serializable {

	private static final long serialVersionUID = -8926658887608629306L;

	private WatchListDescKey watchListDescKey;
	private Account account;
	private String watchListName;
	private String watchListDetails;
	private Long marketDataFrequency;
	private String dataProviders;
	private List<InstId> instIdList;
	private List<String> stockSymbolsList;
	
	private List<OperationParameters> operationParameterses = LazyList.lazyList(new ArrayList<>(),
			FactoryUtils.instantiateFactory(OperationParameters.class));
	
	
	public WatchListDesc() {
		watchListDescKey = new WatchListDescKey();
	}

	public WatchListDesc(Account account) {
		this.account = account;
		this.watchListDescKey = new WatchListDescKey(account.getId());
	}
	
	public WatchListDesc(Long watchListId) {
		this.watchListDescKey = new WatchListDescKey();
		this.watchListDescKey.setWatchListId(watchListId);
	}
	

	public List<String> getStockSymbolsList() {
		return stockSymbolsList;
	}

	public void setStockSymbolsList(List<String> stockSymbolsList) {
		this.stockSymbolsList = stockSymbolsList;
	}

	public void setStockSymbolsListFromOperationList(List<OperationParameters> stockSymbolsList) {
		List<String> stringList = stockSymbolsList.stream().map(OperationParameters::getName)
				.collect(Collectors.toList());
		stringList.forEach(System.out::println);
		this.stockSymbolsList = stringList;
	}

	public List<OperationParameters> getOperationParameterses() {
		return operationParameterses;
	}

	public void setOperationParameterses(List<OperationParameters> operationParameterses) {
		this.operationParameterses = operationParameterses;
	}

	public Long getWatchListId() {
		return watchListDescKey.getWatchListId();
	}

	public Account getAccount() {
		return account;
	}

	public void setAccount(Account account) {
		this.account = account;
		this.getWatchListDescKey().setAccountId(account.getId());
	}

	public String getWatchListName() {
		return watchListName;
	}

	public void setWatchListName(String watchListName) {
		this.watchListName = watchListName;
	}

	public String getWatchListDetails() {
		return watchListDetails;
	}

	public void setWatchListDetails(String watchListDetails) {
		this.watchListDetails = watchListDetails;
	}

	public Long getMarketDataFrequency() {
		return marketDataFrequency;
	}

	public void setMarketDataFrequency(Long marketDataFrequency) {
		this.marketDataFrequency = marketDataFrequency;
	}

	public String getDataProviders() {

		// TODO m it should be List<providerID> like phone in social network
		return dataProviders;
	}

	public void setDataProviders(String dataProviders) {
		this.dataProviders = dataProviders;
	}

	public WatchListDescKey getWatchListDescKey() {
		return watchListDescKey;
	}

	public void setWatchListDescKey(WatchListDescKey watchListDescKey) {
		this.watchListDescKey = watchListDescKey;
	}

	@Override
	public String toString() {
		return "WatchListDesc [watchListDescKey=" + watchListDescKey + ", watchListName="
				+ watchListName + ", watchListDetails=" + watchListDetails + ", marketDataFrequency="
				+ marketDataFrequency + ", dataProviders=" + dataProviders + ", stockSymbolsList=" + stockSymbolsList
				+ ", operationParameterses=" + operationParameterses + "]";
	}

	@Override
	public Long getId() {
		return getWatchListId();
	}

	@Override
	public void setId(Long id) {
		this.watchListDescKey.setWatchListId(id);
	}

	@Override
	public void setWatchListId(Long watchListId) {
		this.getWatchListDescKey().setWatchListId(watchListId);
	}

	public List<InstId> getInstIdList() {
		return instIdList;
	}

	public void setInstIdList(List<InstId> instIdList) {
		this.instIdList = instIdList;
	}
	
	public void setInstIdList() {
		this.instIdList = new ArrayList<>();
		this.getOperationParameterses().forEach(op -> {
			InstId instId = new InstId();
			instId.setInstId(op.getName());
			instId.setWatchListDescId(this.getWatchListId());
			instId.setAccountId(this.getAccount().getId());
			System.out.println(instId.getWatchListDescId());
			
			this.instIdList.add(instId);
		});
	}
}
