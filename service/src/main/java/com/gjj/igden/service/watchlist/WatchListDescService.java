package com.gjj.igden.service.watchlist;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.gjj.igden.dao.daoUtil.DAOException;
import com.gjj.igden.dao.daoimpl.AccountDaoImpl;
import com.gjj.igden.dao.daoimpl.InstIdDaoImpl;
import com.gjj.igden.dao.daoimpl.WatchListDescDaoImpl;
import com.gjj.igden.model.Account;
import com.gjj.igden.model.IWatchListDesc;
import com.gjj.igden.model.WatchListDesc;
import com.gjj.igden.service.util.ServiceException;

@Service
@Transactional(propagation=Propagation.REQUIRED)
public class WatchListDescService {
	
	private static final Logger logger = LoggerFactory.getLogger(ServiceException.class);
	
	@Autowired
	private WatchListDescDaoImpl watchListDescDao;
	
	@Autowired
	private AccountDaoImpl accountDaoImpl;
	
	@Autowired
	private InstIdDaoImpl instIdDaoImpl;
	
	public List<WatchListDesc> getDataSetsAttachedToAcc(Long id) {
		logger.debug("In WatchListDescService, get data set attached to account by id::" + id);
		return watchListDescDao.getDataSetsAttachedToAcc(id);
	}

	public List<String> getStockSymbolsList(Long id) {
		logger.debug("In WatchListDescService, get stock symbol list by id::" + id);
		return instIdDaoImpl.getAllStockSymbols(watchListDescDao.read(id));
	}

	public boolean delete(Long watchListId, Long accountId) throws DAOException {
		logger.debug("In WatchListDescService, Delete watch list where accId is::" + accountId + "and watch list id is::" + watchListId);
		WatchListDesc wl = watchListDescDao.read(watchListId);
		
		if(watchListDescDao.deleteWatchList(wl)) {
			instIdDaoImpl.delete(watchListId, accountId);
		}
		return true;
	}

	public boolean create(WatchListDesc watchListDesc, Long accId) throws DAOException {
		logger.debug("In WatchListDescService, Create watch list ::" + watchListDesc);
		Account account = accountDaoImpl.read(new Account(accId));
		watchListDesc.setStockSymbolsListFromOperationList(watchListDesc.getOperationParameterses());
		
		Long max = watchListDescDao.getMaxIdFromDatabase();

		watchListDesc.setAccount(account);
		watchListDesc.setWatchListId(max+1);
		watchListDesc.setInstIdList();
		
		return watchListDescDao.createWatchListDesc(watchListDesc);
	}

	public boolean update(IWatchListDesc watchListDesc) {
		return watchListDescDao.updateWatchListDesc(watchListDesc);
	}
	
	public IWatchListDesc getWatchListDesc(Long dsId, Long accId) {
		return watchListDescDao.getWatchListDesc(dsId, accId);
	}
}
