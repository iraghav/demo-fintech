package com.gjj.igden.service.test;

import java.util.List;
import java.util.Objects;

import org.junit.Assert;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.gjj.igden.model.Bar;
import com.gjj.igden.model.BarKey;
import com.gjj.igden.service.test.daostub.BarDaoStub;

@Configuration
@ComponentScan(basePackageClasses = {BarDaoStub.class})
class SpringConfigContextBarService {
}

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = SpringConfigContextBarService.class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class BarServiceTest {
  @Autowired
  private BarDaoStub barService;

  @Test
  public void simpleReadTest01() {
    List<Bar> bars = barService.getBarList("AAPL@NASDAQ");
    bars.forEach(System.out::println);
    Assert.assertNotNull(bars);
  }

  @Test
  public void test02_CreateH2DataBaseTest() {
    Bar bar = barService.getSingleBar(1, "AAPL@NASDAQ");
    Assert.assertNotNull(bar);
    Assert.assertEquals("AAPL@NASDAQ", bar.getBarKey().getInstId().toString());
  }

  @Test
  public void test03_Update() {
    Bar bar = barService.getSingleBar(1, "AAPL@NASDAQ");
    Bar barCopy = new Bar();
    BarKey barKey =  new BarKey();
    barKey.setInstId(bar.getBarKey().getInstId());
    barCopy.setBarKey(barKey);
    barCopy.copy(bar);
    barCopy.setLogInfo("test update");
    Assert.assertTrue(barService.updateBar(barCopy));
    bar = barService.getSingleBar(1, "AAPL@NASDAQ");
    Assert.assertEquals("test update", bar.getLogInfo());
  }

  @Test
  public void test04_CreateBar() throws Exception {
    Bar bar = barService.getSingleBar(2, "AAPL@NASDAQ");
    System.out.println(bar);
    Assert.assertNotNull(bar);
    bar.setId(111L);
    System.out.println(bar);
    Assert.assertTrue(barService.createBar(bar));
    List<Bar> barList = barService.getBarList("AAPL@NASDAQ");
    Bar newBar = barList.stream()
      .filter(p -> Objects.equals(p.getId(), 111L)).findAny().get();
    System.out.println(newBar);
    Assert.assertEquals(Long.valueOf(111), newBar.getId());
  }

  @Test
  public void test05_Delete() throws Exception {
    Bar bar = barService.getSingleBar(3, "AAPL@NASDAQ");
    System.out.println(bar);
    Assert.assertNotNull(bar);
    Assert.assertTrue(barService.deleteBar(bar));
    try {
      System.out.println(barService.getSingleBar(3, "AAPL@NASDAQ"));
      Assert.fail();
    } catch (NullPointerException e) {
      Assert.assertTrue(true);
    }
  }
}
