package com.gjj.igden.service.test.daostub;

import com.gjj.igden.dao.WatchListDescDao;
import com.gjj.igden.model.IWatchListDesc;
import com.gjj.igden.model.WatchListDesc;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Component
@SuppressWarnings("unlikely-arg-type")
public class WatchListDescDaoStub implements WatchListDescDao {
  private static Map<Integer, List<IWatchListDesc>> watchListDescsDb;

  static {
	  
	  
	  
		List<IWatchListDesc> watchListDescsAttachedToAccWithIdOne = Stream
				.of(createWatchList(), createWatchList(), createWatchList(), createWatchList())
				.collect(Collectors.toList());
    WatchListDesc theWatchListD = new WatchListDesc();
    theWatchListD.setWatchListName("test-aapl-5minBar-preMarketdata");
    List<IWatchListDesc> watchListDescsAttachedToAccWithIdTwo = Lists.newArrayList(theWatchListD);
    watchListDescsDb = Maps.newHashMap(ImmutableMap
      .of(1, watchListDescsAttachedToAccWithIdOne,
        2, watchListDescsAttachedToAccWithIdTwo));
  }
  
  private static WatchListDesc createWatchList() {
	  WatchListDesc watchListDesc = new WatchListDesc();
	  watchListDesc.getWatchListDescKey().setAccountId(1l);
	  watchListDesc.getWatchListDescKey().setWatchListId(1l);
	  return watchListDesc;
  }

  @Override
  public List<String> getAllStockSymbols(Long id) {
    return Stream.of("C@NYSE", "GS@NYSE").collect(Collectors.toList());
  }

@Override
  public List<IWatchListDesc> getDataSetsAttachedToAcc(Long id) {
    return watchListDescsDb.get(id.intValue());
  }

  @Override
  public void setNamedParamJbd(NamedParameterJdbcTemplate namedParamJbd) {
  }

@Override
  public IWatchListDesc getWatchListDesc(Long dsId, Long accId) {
    return watchListDescsDb.get(accId.intValue()).get(dsId.intValue());
  }

  @Override
  public boolean addTicker(Long watchlistId, String tickerName) {
    return true;
  }

  @Override
  public boolean deleteWatchListDesc(Long dsId, Long accId) {
    return deleteWatchListDesc(watchListDescsDb.get(accId.intValue()).get(dsId.intValue()));
  }



  @Override
  public boolean deleteWatchListDesc(IWatchListDesc watchListDesc) {
    return watchListDescsDb.entrySet()
      .stream()
      .anyMatch(e -> e.getValue()
        .removeIf(p -> p.equals(watchListDesc)));
  }

@Override
  public boolean createWatchListDesc(IWatchListDesc watchListDesc) {
    return watchListDescsDb.get(watchListDesc.getWatchListDescKey().getAccountId().intValue()).add(watchListDesc);
  }

  
@Override
  public boolean updateWatchListDesc(IWatchListDesc watchListDesc) {
    watchListDescsDb.get(watchListDesc.getWatchListDescKey().getAccountId().intValue()).stream()
      .filter(a -> a.equals(watchListDesc))
      .findFirst()
      .ifPresent(p -> p.setWatchListName(watchListDesc.getWatchListName()));
    return true;
  }
}
