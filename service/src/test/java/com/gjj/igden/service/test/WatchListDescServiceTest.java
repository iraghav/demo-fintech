package com.gjj.igden.service.test;

import java.util.List;

import org.junit.Assert;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.gjj.igden.model.Account;
import com.gjj.igden.model.IWatchListDesc;
import com.gjj.igden.service.test.daostub.WatchListDescDaoStub;

@Configuration
@ComponentScan(basePackageClasses = {WatchListDescDaoStub.class})
class SpringTextContext {
}

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = SpringTextContext.class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class WatchListDescServiceTest {
  @Autowired
  private WatchListDescDaoStub watchListDescService;

  @Test
  public void test01_simpleReadTest() throws Exception {
    watchListDescService.getAllStockSymbols(1L).forEach(System.out::println);
  }

  @Test
  public void test02_CreateH2DataBaseTest() {
    List<IWatchListDesc> dataSetList = watchListDescService.getDataSetsAttachedToAcc(1L);
    final int expectedDataSetsAmount = 4;
    Assert.assertEquals(expectedDataSetsAmount, dataSetList.size());
  }

  @Test
  public void test03_ReturnBarList() {
    IWatchListDesc dataSet = watchListDescService.getDataSetsAttachedToAcc(2L).get(0);
    System.out.println(dataSet.getWatchListName());
    Assert.assertNotNull(dataSet);
    Assert.assertEquals("test-aapl-5minBar-preMarketdata", dataSet.getWatchListName());
  }

  @Test
  public void test07_Delete() throws Exception {
    List<IWatchListDesc> dataSetList = watchListDescService.getDataSetsAttachedToAcc(1L);
    final int expectedDataSetsAmount = dataSetList.size();
    boolean deleteResultFlag = watchListDescService.deleteWatchListDesc(dataSetList.get(0).getWatchListDescKey().getWatchListId(),dataSetList.get(0).getWatchListDescKey().getAccountId());
    Assert.assertTrue(deleteResultFlag);
    System.out.println("after deletion ");
    dataSetList = watchListDescService.getDataSetsAttachedToAcc(1L);
    Assert.assertNotEquals(expectedDataSetsAmount, dataSetList.size());
  }

  @Test
  public void test06_CreateDataSet() throws Exception {
    Long accId = 1L;
    IWatchListDesc newWatchList = watchListDescService.getWatchListDesc(1L, accId);
    List<IWatchListDesc> dataSetList = watchListDescService.getDataSetsAttachedToAcc(1L);
    int expectedDataSetsAmountAfterDeletion = 4;
    Assert.assertEquals(expectedDataSetsAmountAfterDeletion, dataSetList.size());
    Assert.assertNotNull(newWatchList);
    newWatchList.setId(111L);
    Account account = new Account();
    account.setId(accId);
   // newWatchList.setAccount(account);
    newWatchList.getWatchListDescKey().setAccountId(account.getId());
    newWatchList.setWatchListName("just testing around");
    Assert.assertTrue(watchListDescService.createWatchListDesc(newWatchList));
    dataSetList = watchListDescService.getDataSetsAttachedToAcc(1L);
    expectedDataSetsAmountAfterDeletion = 5;
    Assert.assertEquals(expectedDataSetsAmountAfterDeletion, dataSetList.size());
  }

  @Test
  public void test05_Update() throws Exception {
    final Long accId = 1L;
    IWatchListDesc dataSet = watchListDescService.getWatchListDesc(1L, accId);
    dataSet.setWatchListName("test update");
    Account account = new Account();
    account.setId(accId);
    dataSet.getWatchListDescKey().setAccountId(account.getId());
    watchListDescService.updateWatchListDesc(dataSet);
    final String dataSetNameDirect = watchListDescService.getWatchListDesc(1L, 1L).getWatchListName();
    Assert.assertEquals("test update", dataSetNameDirect);
  }

  @Test
  public void test04_Read() throws Exception {
    List<IWatchListDesc> watchListDescs = watchListDescService.getDataSetsAttachedToAcc(1L);
    final int size = 4;
    Assert.assertEquals(size,
      watchListDescs.size());
  }

}
