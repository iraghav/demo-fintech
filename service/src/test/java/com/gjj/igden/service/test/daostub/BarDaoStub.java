package com.gjj.igden.service.test.daostub;

import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Component;

import com.gjj.igden.dao.BarDao;
import com.gjj.igden.dao.daoUtil.DAOException;
import com.gjj.igden.model.Bar;
import com.gjj.igden.model.BarKey;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Maps;

@Component
public class BarDaoStub implements BarDao {
  private static Map<Integer, Map<String, List<Bar>>> marketDataDbSimulator;
  private static final int WATCHLIST_ID = 1;

  static {
	  
	 Bar bar = createBar(new BarKey(1L, "AAPL@NASDAQ")) ;
	 bar.setHigh(296.25);
	 
	 Bar bar2 = createBar(new BarKey(2L, "AAPL@NASDAQ")) ;
	 bar.setHigh(296.25);
	 
	 Bar bar3 = createBar(new BarKey(3L, "AAPL@NASDAQ")) ;
	  
    List<Bar> barList1 = Stream.of(bar,bar2,bar3).collect(Collectors.toList());
    
    List<Bar> barList2 = Stream.of(createBar(new BarKey(1L, "GOOG@NASDAQ")),
    		createBar(new BarKey(2L, "GOOG@NASDAQ")),
    		createBar(new BarKey(3L, "GOOG@NASDAQ")), createBar(new BarKey(4L, "GOOG@NASDAQ")))
      .collect(Collectors.toList());
    List<Bar> barListORCL = Stream.of(createBar(new BarKey(1L, "ORCL@NASDAQ")),
    		createBar( new BarKey(2L, "ORCL@NASDAQ")),
    		createBar(new BarKey(3L, "ORCL@NASDAQ")), createBar(new BarKey(4L, "ORCL@NASDAQ")))
      .collect(Collectors.toList());
    Map<String, List<Bar>> barCollectionMap1 = Maps.newHashMap(ImmutableMap.of("AAPL@NASDAQ", barList1, "GOOG@NASDAQ", barList2, "ORCL@NASDAQ", barListORCL));
    Map<String, List<Bar>> barCollectionMap2 = Maps.newHashMap(ImmutableMap.of("ORCL@NASDAQ", barListORCL));
    int simulateDataSetId01 = 1;
    int simulateDataSetId02 = 2;
    marketDataDbSimulator = Maps.newHashMap(ImmutableMap.of(simulateDataSetId01, barCollectionMap1, simulateDataSetId02, barCollectionMap2));
  }
  
  private static Bar createBar(BarKey barkey) {
	  Bar bar = new Bar();
	  bar.setDateTime(new Date());
	  bar.setBarKey(barkey);
	  return bar;
  }

  @Override
  public Bar getSingleBar(long mdId, String instId) {
    return marketDataDbSimulator.get(WATCHLIST_ID).get(instId)
      .stream().filter(p -> (p.getId().equals(mdId))).findFirst().get();
  }

  @Override
  public List<Bar> getBarList(String instId) {
    return marketDataDbSimulator.get(WATCHLIST_ID).get(instId);
  }

  @Override
  public boolean updateBar(Bar bar) {
	/* Bar oldBar = new Bar();
	 oldBar.copy(bar);*/
    marketDataDbSimulator.get(WATCHLIST_ID).get(bar.getBarKey().getInstId().toString())
      .stream().filter(p -> p.getId().equals(bar.getId())).findFirst()
      .ifPresent(m -> {
       /* m.reset();*/
        m.copy(bar);
      });
    return getSingleBar(bar.getId(), bar.getBarKey().getInstId().toString()).equals(bar);
  }

  @Override
  public boolean createBar(Bar bar) throws DAOException {
    return marketDataDbSimulator.get(WATCHLIST_ID).get(bar.getBarKey().getInstId().toString()).add(bar);
  }

  @Override
  public boolean deleteBar(long mdId, String instId) {
    marketDataDbSimulator.get(WATCHLIST_ID).get(instId).stream()
      .filter(p -> p.getId().equals(mdId))
      .findFirst().ifPresent(Bar::reset);
    return true;
  }

  @Override
  public boolean deleteBar(Bar bar) {
    return deleteBar(bar.getId(), bar.getBarKey().getInstId().toString());
  }

  @Override
  public List<String> searchTickersByChars(String tickerNamePart) {
    return null;
  }

  @Override
  public void setNamedParamJbd(NamedParameterJdbcTemplate namedParamJbd) {
    System.out.println("doing nothing here...");
  }
}
