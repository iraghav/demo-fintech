
package com.gjj.igden.service.test;

import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.gjj.igden.model.Account;
import com.gjj.igden.model.WatchListDesc;
import com.gjj.igden.service.test.daostub.AccountDaoStub;


@Configuration
@ComponentScan(basePackageClasses = {
  AccountDaoStub.class})
class SpringConfigContextAccountService {
}

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = SpringConfigContextAccountService.class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class AccountServiceTest {
  @Autowired
  AccountDaoStub accountService;
  
  
  @Test
  public void test01_readAll() {
      List<Account> accounts = accountService.getAllAccounts();
      Assert.assertNotNull(accounts);
  }
  
  @Test
  public void test02_readAll()  {
    List<Account> accounts = accountService.getAllAccounts();
    accounts.forEach(System.out::println);
    System.out.println("==========");
    System.out.println("==========");
    List<WatchListDesc> watchListDescs = new ArrayList<>(1);
    accounts.stream()
      .filter(p -> p.getAccountName().equals("accountName_test2")).findAny()
      .ifPresent(p -> watchListDescs.addAll(p.getAttachedWatchedLists()));
    System.out.println(watchListDescs.size());
    Assert.assertEquals(1, watchListDescs.size());
    Assert.assertTrue(accounts.stream()
      .anyMatch(p -> p.getAccountName().equals("accountName_test1")));
  }
  
  
  @Test
  public void test03_EditData()  {
    Account account = accountService.getAccountById(2l);
    String oldName = account.getAccountName();
    account.setAccountName("test update");
    accountService.update(account);
    String newName = accountService.getAccountById(2l).getAccountName();
    Assert.assertNotEquals(newName, oldName);
  }
  
  @Test
  public void test04_Create()  {
    Account account = accountService.getAccountById((long) 1);
    account.setId((long) 111);
    account.setAccountName(" ***** new Account ****** ");
    int oldSize = accountService.getAllAccounts().size();
    accountService.create(account);
    int newSize = accountService.getAllAccounts().size();
    List<Account> accounts = accountService.getAllAccounts();
    accounts.forEach(System.out::println);
    Assert.assertNotEquals(oldSize, newSize);
  }
  
  @Test
  public void test05_SimpleDelete()  {
    int oldSize = accountService.getAllAccounts().size();
    boolean flag = accountService.delete(1L);
    Assert.assertTrue(flag);
    int newSize = accountService.getAllAccounts().size();
    Assert.assertNotEquals(oldSize, newSize);
  }
 
}

